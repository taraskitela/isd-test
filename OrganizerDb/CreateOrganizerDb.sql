USE master
GO
IF NOT EXISTS (
   SELECT name
   FROM sys.databases
   WHERE name = N'OrganizerDb'
)
CREATE DATABASE OrganizerDb
GO

USE OrganizerDb
GO

CREATE TABLE dbo.Users(
	Id INT IDENTITY(1,1) NOT NULL  PRIMARY KEY,
	Name [NVARCHAR](50)  NOT NULL
);
GO

USE OrganizerDb
IF OBJECT_ID('dbo.Events', 'U') IS NOT NULL
DROP TABLE dbo.Events
GO

CREATE TABLE dbo.Events
(
   Id        INT  IDENTITY(1,1) NOT NULL PRIMARY KEY ,
   Name      [NVARCHAR](50)  NOT NULL,
   Description  [NVARCHAR](MAX)  NOT NULL,
   DateSheduled [DATETIME]  NOT NULL,
   UserId INT ,
   FOREIGN KEY (UserId)  REFERENCES Users (Id)
);
GO