using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OrganizerClient.Inrefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using OrganizerClient.Services;


namespace OrganizerClient
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        /// 
        

        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var serviceProvider = new ServiceCollection()
                                .AddSingleton<IBaseService, BaseService>()
                                .AddSingleton<IConfigProvider , JsonConfigurationProvider>()
                                .AddSingleton<IRemoteServiceClient, WebClientService>()
                                .AddSingleton<IEventService , EventService>() 
                                .AddSingleton<IAuthService , UserAuthService>()
                                .AddSingleton<ILogger ,UIMessageLogger>()
                                .BuildServiceProvider();
            
            Application.Run(new WinFormsUI(
                serviceProvider.GetService<IEventService>() , 
                serviceProvider.GetService<ILogger>()));
        }
    }
}
