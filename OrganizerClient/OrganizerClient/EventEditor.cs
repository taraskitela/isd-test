﻿using System;
using System.Windows.Forms;
using OrganizerClient.DataModels;

namespace OrganizerClient
{
    public partial class EventEditor : Form
    {
        public EventModel Event { get; private set; } 
        public EventEditor()
        {
            InitializeComponent();

            Event = new EventModel();
        }

        public EventEditor(EventModel existingEventModel)
        {
            InitializeComponent();

            Event = new EventModel() {
                DateSheduled = existingEventModel.DateSheduled,
                Description = existingEventModel.Description,
                Name = existingEventModel.Name,
                Id = existingEventModel.Id
            };

            this.eventName.Text = Event.Name ;
            this.eventDescription.Text = Event.Description ;
            this.dateSheduled.SelectionStart = Event.DateSheduled;

        }

        private bool IsEventValid(EventModel eventModel)
        {
            var isValid = !string.IsNullOrEmpty(eventModel.Name) && !string.IsNullOrEmpty(eventModel.Description) &&
                !string.IsNullOrWhiteSpace(eventModel.Name) && !string.IsNullOrWhiteSpace(eventModel.Description);

            return isValid;
        }

        private void Save_button_Click(object sender, EventArgs e)
        {
            Event.Name = this.eventName.Text;
            Event.Description = this.eventDescription.Text;
            Event.DateSheduled = this.dateSheduled.SelectionStart;

            if(!IsEventValid(Event))
            {
                MessageBox.Show("Invalid event");
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void cancell_button_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
