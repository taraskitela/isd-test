﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrganizerClient.Exeptions
{
    class NetworkException : Exception
    {
        public NetworkException(string message) : base(message)
        {
        }
    }
}
