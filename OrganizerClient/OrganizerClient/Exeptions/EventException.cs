﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrganizerClient.Exeptions
{
    class EventException : Exception
    {
        public EventException(string message) : base(message)
        {
        }
    }
}
