﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrganizerClient.Exeptions
{
    class BaseServiceException:Exception
    {
        public BaseServiceException(string message) : base(message)
        {
        }
    }
}
