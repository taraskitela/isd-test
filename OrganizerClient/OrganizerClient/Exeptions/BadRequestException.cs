﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrganizerClient.Exeptions
{
    class BadRequestException : Exception
    {
        public BadRequestException(string message) : base(message)
        {
        }
    }
}
