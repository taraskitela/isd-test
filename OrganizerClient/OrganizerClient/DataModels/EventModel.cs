﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace OrganizerClient.DataModels
{
    public class EventModel : INotifyPropertyChanged
    {
        private string _name;
        public string Name {
            get
            {
                return this._name;
            }
            set
            {
                if (value != this._name)
                {
                    this._name = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _description;
        public string Description {
            get
            {
                return this._description;
            }
            set
            {
                if (value != this._description)
                {
                    this._description = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public DateTime _dateSheduled;
        public DateTime DateSheduled {

            get
            {
                return this._dateSheduled;
            }
            set
            {
                if (value != this._dateSheduled)
                {
                    this._dateSheduled = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private int _id;
        public int Id {
            get
            {
                return this._id;
            }
            set
            {
                if (value != this._id)
                {
                    this._id = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
