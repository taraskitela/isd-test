﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrganizerClient.Inrefaces
{
    /// <summary>
    /// Intarface to configuration provider
    /// </summary>
    interface IConfigProvider
    {
        Dictionary<string, string> Conf { get; }

        void SaveChanges();
    }
}
