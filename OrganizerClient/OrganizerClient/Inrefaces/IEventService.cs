﻿using OrganizerClient.DataModels;
using System.ComponentModel;
using System.Threading.Tasks;

namespace OrganizerClient.Inrefaces
{
    /// <summary>
    /// Interface to busines layer
    /// </summary>
    public interface IEventService
    {
        BindingList<EventModel> Events { get; set; }

        Task AddNewEventAsync(EventModel newEvent);
        Task UpdateEvent(EventModel editedEvent, int index);
        Task DeleteEvent(int index);
        Task UpdateEventsList();
    }

}
