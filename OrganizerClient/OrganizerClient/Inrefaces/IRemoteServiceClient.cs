﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OrganizerClient.Inrefaces
{
    public interface IRemoteServiceClient
    {
        Task<string> SendAsync(HttpMethod httpMethod, Uri serverEndpoint, string content);
    }
}
