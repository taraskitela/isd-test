﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OrganizerClient.Inrefaces
{
    public interface IBaseService
    {
        Task<string> ProcessRequestAsync(HttpMethod httpMethod, string endpoint, string data = null);       
    }
}
