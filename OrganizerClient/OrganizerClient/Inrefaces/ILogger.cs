﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrganizerClient.Inrefaces
{
    /// <summary>
    /// Intarface to logger layer
    /// </summary>
    public interface ILogger
    {
        void Log(Exception exc);
    }
}
