﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using OrganizerClient.Exeptions;
using OrganizerClient.Inrefaces;

namespace OrganizerClient
{
    public class WebClientService : IRemoteServiceClient
    {
        private HttpClient _httpClient = new HttpClient();

        #region Support IRemoteServiceClient

        public async Task<string> SendAsync(HttpMethod httpMethod, Uri serverEndpoint, string content)
        {
            HttpResponseMessage resp = default;
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(
                    httpMethod,
                    serverEndpoint);

                if (!string.IsNullOrEmpty(content))
                    request.Content = new StringContent(content, Encoding.UTF8, "application/json");                 

                resp = await _httpClient.SendAsync(request);
                resp.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException)
            {
                HandleRequestException(resp);
            }
            return resp.Content.ReadAsStringAsync().Result;
        }

        private void HandleRequestException(HttpResponseMessage resp)
        {
            if (resp == null)
                throw new NetworkException("Bad gateway.");

            if (resp.StatusCode == HttpStatusCode.BadRequest)
                throw new BadRequestException("Invalid request data.");
            else
                throw new NetworkException("Some network problems.");
        }

        #endregion
    }
}



