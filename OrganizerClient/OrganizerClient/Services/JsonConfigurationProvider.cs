﻿using OrganizerClient.Inrefaces;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace OrganizerClient.Services
{
    class JsonConfigurationProvider : IConfigProvider
    {
        private Dictionary<string, string> _conf;
        public Dictionary<string, string> Conf { get { return _conf; } }

        public JsonConfigurationProvider()
        {
            try
            {
                var jsonString = File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "config.json"));

                _conf = JsonSerializer.Deserialize<Dictionary<string, string>>(jsonString);

            }
            catch (FileNotFoundException)
            {
                using (File.Create(Path.Combine(Directory.GetCurrentDirectory(), "config.json"))) 
                _conf = new Dictionary<string, string>();
            }
        }

        public void SaveChanges()
        {
            File.WriteAllText(Path.Combine(Directory.GetCurrentDirectory(), "config.json"),
                JsonSerializer.Serialize<Dictionary<string, string>>(_conf));
        }
    }
}
