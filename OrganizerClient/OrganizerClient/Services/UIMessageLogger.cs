﻿using System;
using System.Windows.Forms;
using OrganizerClient.Inrefaces;

namespace OrganizerClient.Services
{
    class UIMessageLogger : ILogger
    {
        public void Log(Exception exc)
        {
            MessageBox.Show(exc.Message);
        }
    }
}
