﻿using System;
using OrganizerClient.DataModels;
using OrganizerClient.Inrefaces;

namespace OrganizerClient.Services
{
    class UserAuthService : IAuthService
    {
        private IConfigProvider _configProvider;
        private UserModel _user;
        public static string UserId { get; set; }

        public UserAuthService(IConfigProvider configProvider)
        {
            _configProvider = configProvider;
        }

        public void Auth()
        {
            if (!_configProvider.Conf.TryGetValue("userId", out _))
            {
                _configProvider.Conf.Add(
                    "userId",
                    GenerateUniqueUserId());

                _configProvider.SaveChanges();
            }

            _user = new UserModel() { UniqueId = _configProvider.Conf["userId"] };
            UserId = _user.UniqueId;
        }

        private string GenerateUniqueUserId()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
