﻿using System.ComponentModel;
using System.Net.Http;
using System.Threading.Tasks;
using OrganizerClient.DataModels;
using OrganizerClient.Exeptions;
using System.Text.Json;
using OrganizerClient.Inrefaces;
using OrganizerClient.Services;
using OrganizerClient.Constants;
using System.Collections.Generic;

namespace OrganizerClient
{
    class EventService : IEventService
    {
        private IBaseService _baseService;
        private BindingList<EventModel> _Events = new BindingList<EventModel>();

        public EventService(IBaseService baseService)
        {
            _baseService = baseService;
        }

        #region Support IEventService
        public BindingList<EventModel> Events
        {
            get { return _Events; }
            set { _Events = value; }
        }

        public async Task AddNewEventAsync(EventModel newEvent)
        {
            try
            {
                await _baseService.ProcessRequestAsync(
                    HttpMethod.Post, 
                    string.Format(ServerEndpoints.EventAdd, UserAuthService.UserId), 
                    JsonSerializer.Serialize<EventModel>(newEvent));
            }
            catch (BaseServiceException e)
            {
                throw new EventException($"Add new event failed: {e.Message}");
            }
        }

        public async Task UpdateEvent(EventModel editedEvent, int index)
        {
            try
            {
                await _baseService.ProcessRequestAsync(
                    HttpMethod.Put,
                    string.Format(ServerEndpoints.EventUpdate, UserAuthService.UserId),
                    JsonSerializer.Serialize<EventModel>(editedEvent));
            }
            catch (BaseServiceException e)
            {
                throw new EventException($"Update event failed: {e.Message}");
            }   
        }

        public async Task DeleteEvent(int index)
        {
            try
            {
                await _baseService.ProcessRequestAsync(
                    HttpMethod.Delete,
                    string.Format(ServerEndpoints.EventDelete, Events[index].Id, UserAuthService.UserId));
            }
            catch (BaseServiceException e)
            {
                throw new EventException($"Delete event failed: {e.Message}");
            }   
        }

        public async Task UpdateEventsList()
        {
            try
            {
                var response = await _baseService.ProcessRequestAsync(
                    HttpMethod.Get,
                    string.Format(ServerEndpoints.GetEventsList, UserAuthService.UserId)
                    );

                var actualEvents =  JsonSerializer.Deserialize<List<EventModel>>(response);
                this.Events.Clear();

                foreach (EventModel ev in actualEvents)
                {
                    this.Events.Add(ev);
                }
            }
            catch (BaseServiceException e)
            {
                throw new EventException($"Get events list failed: {e.Message}");
            }
        }
        #endregion
    }
}
