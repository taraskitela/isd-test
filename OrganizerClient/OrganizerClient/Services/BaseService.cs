﻿using OrganizerClient.Exeptions;
using OrganizerClient.Inrefaces;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace OrganizerClient.Services
{
    class BaseService : IBaseService
    {
        private IRemoteServiceClient _webClient;
        private IConfigProvider _configProvider;
        private IAuthService _authService;

        private string _host;

        public BaseService(IRemoteServiceClient webClient , IConfigProvider configProvider , IAuthService authService)
        {
            _webClient = webClient;
            _configProvider = configProvider;
            _authService = authService;

            GetServerConfiguration();
            _authService.Auth();
        }

        public async Task<string> ProcessRequestAsync(HttpMethod httpMethod, string endpoint, string data = null)         
        {
            try
            {
                return await _webClient.SendAsync(httpMethod,
                  new Uri(new Uri(_host) , endpoint),
                    data);
            }
            catch (BadRequestException e)
            {
                throw new BaseServiceException(e.Message);
            }
            catch (NetworkException e)
            {
                throw new BaseServiceException(e.Message);
            }
            catch (Exception e) 
            {
                throw new BaseServiceException (e.Message);
            }
        }

        private void GetServerConfiguration() 
        {
            if (!_configProvider.Conf.TryGetValue("host", out _host))
            {
                _host = "http://localhost:61017";
                _configProvider.Conf.Add(
                    "host",
                    _host);
                _configProvider.SaveChanges();
            }
        }
    }
}
