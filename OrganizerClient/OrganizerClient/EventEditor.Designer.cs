﻿
namespace OrganizerClient
{
    partial class EventEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Save_button = new System.Windows.Forms.Button();
            this.eventDescription = new System.Windows.Forms.TextBox();
            this.dateSheduled = new System.Windows.Forms.MonthCalendar();
            this.eventName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cancell_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Save_button
            // 
            this.Save_button.Location = new System.Drawing.Point(495, 362);
            this.Save_button.Name = "Save_button";
            this.Save_button.Size = new System.Drawing.Size(215, 23);
            this.Save_button.TabIndex = 0;
            this.Save_button.Text = "Save";
            this.Save_button.UseVisualStyleBackColor = true;
            this.Save_button.Click += new System.EventHandler(this.Save_button_Click);
            // 
            // eventDescription
            // 
            this.eventDescription.Location = new System.Drawing.Point(18, 64);
            this.eventDescription.Multiline = true;
            this.eventDescription.Name = "eventDescription";
            this.eventDescription.Size = new System.Drawing.Size(704, 115);
            this.eventDescription.TabIndex = 1;
            // 
            // dateSheduled
            // 
            this.dateSheduled.Location = new System.Drawing.Point(18, 241);
            this.dateSheduled.Name = "dateSheduled";
            this.dateSheduled.TabIndex = 2;
            // 
            // eventName
            // 
            this.eventName.Location = new System.Drawing.Point(18, 21);
            this.eventName.Name = "eventName";
            this.eventName.Size = new System.Drawing.Size(704, 23);
            this.eventName.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Event name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Event description";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 217);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Event date scheduled";
            // 
            // cancell_button
            // 
            this.cancell_button.Location = new System.Drawing.Point(235, 362);
            this.cancell_button.Name = "cancell_button";
            this.cancell_button.Size = new System.Drawing.Size(215, 23);
            this.cancell_button.TabIndex = 7;
            this.cancell_button.Text = "Cancel";
            this.cancell_button.UseVisualStyleBackColor = true;
            this.cancell_button.Click += new System.EventHandler(this.cancell_button_Click);
            // 
            // EventEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 421);
            this.ControlBox = false;
            this.Controls.Add(this.cancell_button);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.eventName);
            this.Controls.Add(this.dateSheduled);
            this.Controls.Add(this.eventDescription);
            this.Controls.Add(this.Save_button);
            this.Name = "EventEditor";
            this.Text = "EventEditor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Save_button;
        private System.Windows.Forms.TextBox eventDescription;
        private System.Windows.Forms.MonthCalendar dateSheduled;
        private System.Windows.Forms.TextBox eventName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button cancell_button;
    }
}