﻿using System.Windows.Forms;

namespace OrganizerClient
{
    partial class WinFormsUI
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddEvent_button = new System.Windows.Forms.Button();
            this.events_dataGridView = new System.Windows.Forms.DataGridView();
            this.EditEvent_button = new System.Windows.Forms.Button();
            this.DeleteEvent_button = new System.Windows.Forms.Button();
            this.eventName_textBox = new System.Windows.Forms.TextBox();
            this.evenDateSheduled_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.events_dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // AddEvent_button
            // 
            this.AddEvent_button.Location = new System.Drawing.Point(24, 92);
            this.AddEvent_button.Name = "AddEvent_button";
            this.AddEvent_button.Size = new System.Drawing.Size(120, 23);
            this.AddEvent_button.TabIndex = 1;
            this.AddEvent_button.Text = "Add";
            this.AddEvent_button.UseVisualStyleBackColor = true;
            this.AddEvent_button.Click += new System.EventHandler(this.AddEvent_button_Click);
            // 
            // events_dataGridView
            // 
            this.events_dataGridView.AllowUserToAddRows = false;
            this.events_dataGridView.AllowUserToDeleteRows = false;
            this.events_dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.events_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.events_dataGridView.Location = new System.Drawing.Point(12, 133);
            this.events_dataGridView.MultiSelect = false;
            this.events_dataGridView.Name = "events_dataGridView";
            this.events_dataGridView.ReadOnly = true;
            this.events_dataGridView.RowTemplate.Height = 25;
            this.events_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.events_dataGridView.Size = new System.Drawing.Size(777, 305);
            this.events_dataGridView.TabIndex = 2;
            this.events_dataGridView.SelectionChanged += new System.EventHandler(this.events_dataGridView_SelectionChanged);
            // 
            // EditEvent_button
            // 
            this.EditEvent_button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EditEvent_button.Location = new System.Drawing.Point(343, 92);
            this.EditEvent_button.Name = "EditEvent_button";
            this.EditEvent_button.Size = new System.Drawing.Size(121, 23);
            this.EditEvent_button.TabIndex = 3;
            this.EditEvent_button.Text = "Edit";
            this.EditEvent_button.UseVisualStyleBackColor = true;
            this.EditEvent_button.Click += new System.EventHandler(this.EditEvent_button_Click);
            // 
            // DeleteEvent_button
            // 
            this.DeleteEvent_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DeleteEvent_button.Location = new System.Drawing.Point(669, 92);
            this.DeleteEvent_button.Name = "DeleteEvent_button";
            this.DeleteEvent_button.Size = new System.Drawing.Size(120, 23);
            this.DeleteEvent_button.TabIndex = 4;
            this.DeleteEvent_button.Text = "Delete";
            this.DeleteEvent_button.UseVisualStyleBackColor = true;
            this.DeleteEvent_button.Click += new System.EventHandler(this.DeleteEvent_button_Click);
            // 
            // eventName_textBox
            // 
            this.eventName_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.eventName_textBox.Location = new System.Drawing.Point(24, 40);
            this.eventName_textBox.Name = "eventName_textBox";
            this.eventName_textBox.ReadOnly = true;
            this.eventName_textBox.Size = new System.Drawing.Size(440, 23);
            this.eventName_textBox.TabIndex = 5;
            // 
            // evenDateSheduled_textBox
            // 
            this.evenDateSheduled_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.evenDateSheduled_textBox.Location = new System.Drawing.Point(533, 41);
            this.evenDateSheduled_textBox.Name = "evenDateSheduled_textBox";
            this.evenDateSheduled_textBox.ReadOnly = true;
            this.evenDateSheduled_textBox.Size = new System.Drawing.Size(256, 23);
            this.evenDateSheduled_textBox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "Event name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(533, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "Event date sheduled";
            // 
            // WinFormsUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.evenDateSheduled_textBox);
            this.Controls.Add(this.eventName_textBox);
            this.Controls.Add(this.DeleteEvent_button);
            this.Controls.Add(this.EditEvent_button);
            this.Controls.Add(this.events_dataGridView);
            this.Controls.Add(this.AddEvent_button);
            this.Name = "WinFormsUI";
            this.Text = "Organizer";
            this.Load += new System.EventHandler(this.WinFormsUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.events_dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button AddEvent_button;
        private System.Windows.Forms.DataGridView events_dataGridView;
        private Button EditEvent_button;
        private Button DeleteEvent_button;
        private TextBox eventName_textBox;
        private TextBox evenDateSheduled_textBox;
        private Label label1;
        private Label label2;
    }
}

