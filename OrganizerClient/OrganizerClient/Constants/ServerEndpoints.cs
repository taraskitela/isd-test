﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrganizerClient.Constants
{
    static class ServerEndpoints
    {
        public static string EventAdd => "api/events/add?userId={0}";
        public static string EventUpdate => "api/events/update?userId={0}";
        public static string EventDelete => "api/events/{0}/delete?userId={1}";
        public static string GetEventsList => "api/events?userId={0}";
    }
}
