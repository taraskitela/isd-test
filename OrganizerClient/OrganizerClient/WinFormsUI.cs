﻿using System;
using System.Windows.Forms;
using OrganizerClient.Exeptions;
using OrganizerClient.Inrefaces;

namespace OrganizerClient
{
    public partial class WinFormsUI : Form
    {
        private readonly ILogger _logger;
        private readonly IEventService _eventService;
        private BindingSource _eventsBindingSource = new BindingSource();

        public WinFormsUI(IEventService eventService, ILogger logger)
        {
            _logger = logger;
            _eventService = eventService;

            InitializeComponent();

            events_dataGridView.AutoGenerateColumns = false;

            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Description";
            column.Name = "Event description";
            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            events_dataGridView.Columns.Add(column);
        }

        private async void WinFormsUI_Load(object sender, EventArgs e)
        {
            _eventsBindingSource.DataSource = _eventService.Events;
            events_dataGridView.DataSource = _eventsBindingSource;

            try
            {
                LoadingSpinner(true);
                await _eventService.UpdateEventsList();
                LoadingSpinner(false);
            }
            catch (Exception ex)
            {
                _logger.Log(ex);
            }
        }

        private async void AddEvent_button_Click(object sender, EventArgs e)
        {
            var eventEditor = new EventEditor();
            if (eventEditor.ShowDialog() == DialogResult.OK)
            {
                LoadingSpinner(true, (sender as Button));
                try
                {
                    await _eventService.AddNewEventAsync(eventEditor.Event);
                    await _eventService.UpdateEventsList();
                }
                catch (EventException ex)
                {
                    _logger.Log(ex);
                }
                finally
                {
                    LoadingSpinner(false, (sender as Button));
                }
            }
        }

        private async void EditEvent_button_Click(object sender, EventArgs e)
        {
            if (events_dataGridView.RowCount == 0)
                return;

            var eventEditor = new EventEditor(_eventService.Events[events_dataGridView.SelectedRows[0].Index]);
            if (eventEditor.ShowDialog() == DialogResult.OK)
            {
                LoadingSpinner(true, (sender as Button));
                try
                {
                    await _eventService.UpdateEvent(eventEditor.Event,
                        events_dataGridView.SelectedRows[0].Index);
                    await _eventService.UpdateEventsList();
                }
                catch (EventException ex)
                {
                    _logger.Log(ex);
                }
                finally
                {
                    LoadingSpinner(false, (sender as Button));
                }
            }
        }

        private async void DeleteEvent_button_Click(object sender, EventArgs e)
        {
            if (events_dataGridView.RowCount == 0)
                return;

            LoadingSpinner(true, (sender as Button));
            try
            {
                await _eventService.DeleteEvent(events_dataGridView.SelectedRows[0].Index);
                await _eventService.UpdateEventsList();
            }
            catch (EventException ex)
            {
                _logger.Log(ex);
            }
            finally
            {
                LoadingSpinner(false, (sender as Button));
            }
        }

        private void LoadingSpinner(bool enabled, Button btn = null)
        {
            if (btn != null)
                btn.Enabled = !enabled;
            this.Cursor = enabled ? Cursors.WaitCursor : Cursors.Default;
        }

        private void events_dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            var selectedRows = events_dataGridView.SelectedRows;
            if (selectedRows.Count > 0)
            {
                var index = events_dataGridView.SelectedRows[0].Index;
                eventName_textBox.Text = _eventService.Events[index].Name;
                evenDateSheduled_textBox.Text = _eventService.Events[index].DateSheduled.Date.ToString("d");
            }
            else
            {
                eventName_textBox.Text = string.Empty;
                evenDateSheduled_textBox.Text = string.Empty;
            }
        }
    }
}
