﻿using System;

namespace OrganizerWebApi.DataModels
{
    public class EventModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateSheduled { get; set; }
    }
}
