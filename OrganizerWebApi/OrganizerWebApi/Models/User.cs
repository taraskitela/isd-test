﻿using System;
using System.Collections.Generic;

#nullable disable

namespace OrganizerWebApi.Models
{
    public partial class User
    {
        public User()
        {
            Events = new HashSet<Event>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Event> Events { get; set; }
    }
}
