﻿using System;
using System.Collections.Generic;

#nullable disable

namespace OrganizerWebApi.Models
{
    public partial class Event
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateSheduled { get; set; }
        public int? UserId { get; set; }

        public virtual User User { get; set; }
    }
}
