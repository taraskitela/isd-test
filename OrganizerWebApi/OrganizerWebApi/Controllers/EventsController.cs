﻿using Microsoft.AspNetCore.Mvc;
using OrganizerWebApi.DataModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OrganizerWebApi.Interfaces;

namespace OrganizerWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {

        private IEventService _eventService;
        public EventsController(IEventService eventService)
        {
            _eventService = eventService;            
        }

        //GET api/events?userId={0}
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EventModel>>> GetEvents([FromQuery] string userId)
        {            
            try
            {
                var eventSet = await _eventService.GetEventsList(userId);
                return Ok(eventSet);
            }
            catch(Exception e)
            {
                return Problem(e.Message);
            }           
        }

        //POST api/events/add?userId={userId}
        [HttpPost]
        [Route("add")]
        public async Task<IActionResult> Add([FromBody] EventModel eventModel , [FromQuery] string userId)
        {
            try
            {
                await _eventService.CreateEvent(eventModel , userId);
                return Ok();
            }
            catch (Exception e)
            {
                return Problem(e.Message);
            }
        }

        // PUT api/events/update?userId={userId}
        [HttpPut]
        [Route("update")]
        public async Task<IActionResult> Update([FromQuery] string userId, [FromBody] EventModel eventModel)
        {
            try
            {
                await _eventService.UpdateEvent(eventModel, userId);
                return Ok();
            }
            catch (Exception e)
            {
                return Problem(e.Message);
            }
        }

        // DELETE api/events/{eventId}/delete?userId={userId}
        [HttpDelete]
        [Route("{eventId}/delete")]
        public async Task<IActionResult> Delete(int eventId, [FromQuery] string userId)
        {
            try
            {
                await _eventService.DeleteEvent(eventId, userId);
                return Ok();
            }
            catch (Exception e)
            {
                return Problem(e.Message);
            }
        }
    }
}
