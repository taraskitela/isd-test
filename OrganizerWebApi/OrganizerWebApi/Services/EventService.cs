﻿using OrganizerWebApi.DataModels;
using OrganizerWebApi.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrganizerWebApi.Services
{
    /// <summary>
    ///  Events business layer
    /// </summary>
    public class EventService: IEventService 
    {
        private readonly IRepository<EventModel> _eventRepository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventRepository">Events data layer</param>
        public EventService(IRepository<EventModel> eventRepository)
        {
            _eventRepository = eventRepository;
        }

        #region Support IEventService
        public Task CreateEvent(EventModel item, string owner)
        {
            return _eventRepository.Create(item , owner);
        }
        public Task DeleteEvent(int id, string owner)
        {
            return _eventRepository.Delete(id,owner);
        }
        public Task<IEnumerable<EventModel>> GetEventsList(string owner)
        {
            return _eventRepository.GetItemList(owner);
        }
        public Task UpdateEvent(EventModel item, string owner)
        {
            return _eventRepository.Update(item , owner);
        }
        #endregion
    }
}
