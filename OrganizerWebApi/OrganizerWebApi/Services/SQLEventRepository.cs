﻿
using OrganizerWebApi.DataModels;
using OrganizerWebApi.Interfaces;
using OrganizerWebApi.Models;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace OrganizerWebApi.Services
{
    public class SQLEventRepository : IRepository<EventModel>
    {
        private OrganizerDbContext _dbContext;

        public SQLEventRepository(OrganizerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        private int GetUserId(string userName)
        {
            User x = _dbContext.Users.FirstOrDefault(x => x.Name == userName);
            if (x == null)
            {
                User newUser = new User() { Name = userName };
                _dbContext.Users.Add(newUser);
                _dbContext.SaveChanges();
            }

            return _dbContext.Users.FirstOrDefault(x => x.Name == userName).Id;
        }

        #region Support IRepositoryService
        public async Task<IEnumerable<EventModel>> GetItemList(string owner)
        {
            try
            {
                var eventSet = await _dbContext.Events
                    .Where(ev => ev.User.Name == owner)
                    .Select(x => new EventModel()
                    {
                        Name = x.Name,
                        DateSheduled = x.DateSheduled,
                        Description = x.Description,
                        Id = x.Id
                    }).ToListAsync();

                return eventSet;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public async Task Create(EventModel item, string owner)
        {
            Event newEvent = new Event()
            {
                Name = item.Name,
                DateSheduled = item.DateSheduled,
                Description = item.Description,
                UserId = GetUserId(owner)
            };

            _dbContext.Events.Add(newEvent);
            await _dbContext.SaveChangesAsync();
        }
        public async Task Update(EventModel item, string owner)
        {
            var oldEvent = _dbContext.Events.Where(ev => ev.User.Name == owner && ev.Id == item.Id).Single();

            oldEvent.DateSheduled = item.DateSheduled;
            oldEvent.Description = item.Description;
            oldEvent.Name = item.Name;

            await _dbContext.SaveChangesAsync();
        }
        public async Task Delete(int id, string owner)
        {
            var removedEvent = _dbContext.Events.Where(ev => ev.User.Name == owner && ev.Id == id).Single();

            if (removedEvent != null)
            {
                _dbContext.Events.Remove(removedEvent);
                await _dbContext.SaveChangesAsync();
            }
        }

        #region Support IDisposable
        private bool disposedValue;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: освободить управляемое состояние (управляемые объекты)
                }

                // TODO: освободить неуправляемые ресурсы (неуправляемые объекты) и переопределить метод завершения
                // TODO: установить значение NULL для больших полей
                disposedValue = true;
            }
        }

        // // TODO: переопределить метод завершения, только если "Dispose(bool disposing)" содержит код для освобождения неуправляемых ресурсов
        // ~SQLEventRepositoryService()
        // {
        //     // Не изменяйте этот код. Разместите код очистки в методе "Dispose(bool disposing)".
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки в методе "Dispose(bool disposing)".
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
