﻿using OrganizerWebApi.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrganizerWebApi.Interfaces
{
    public interface IRepository<T> : IDisposable
        where T : class
    {
        Task<IEnumerable<T>> GetItemList(string owner);       
        Task Create(T item, string owner);
        Task Update(T item, string owner);
        Task Delete(int id, string owner);
        
    }
}
