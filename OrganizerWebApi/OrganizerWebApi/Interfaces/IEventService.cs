﻿using OrganizerWebApi.DataModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrganizerWebApi.Interfaces
{
    public interface IEventService
    {
        Task CreateEvent(EventModel item, string owner);
        Task UpdateEvent(EventModel item, string owner);
        Task DeleteEvent(int id, string owner);
        Task<IEnumerable<EventModel>> GetEventsList(string owner);
    }
}
