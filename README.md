# README #

This README created for ISD Test projects.

### What is this repository for? ###

* Organizer WinForm .NET Core Client
* Organizer .NET Core WebAPI
* OrganizerDb MSSQL script. 

### How do I get set up? ###

* Database configuration:
	* Create database using OrganizerDb/CreateOrganizerDb.sql script
* WebAPI configuration:
	* Run the OrganizerWebApi project by IIS Express
* Client configuration:
	* Use the OrganizerClient\OrganizerClient\config.json for the WebApi host and User auth configuration 